import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-dark bg-success">
        <div className="container-fluid">
          <NavLink className="text-decoration-underline navbar-brand" to="/">CarCar</NavLink>
          <h5>||</h5>
          <div className="text-left dropdown">
            <button className="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
              Sales
            </button>
            <div className="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton2">
              <li><NavLink className="dropdown-item" aria-current="page" to="customers/new/">New Customers</NavLink></li>
              <li><NavLink className="dropdown-item" aria-current="page" to="sales-persons/new/">New Sales Representative</NavLink></li>
              <li><NavLink className="dropdown-item" aria-current="page" to="sales-records/new/">New Sales Records</NavLink></li>
              <li><NavLink className="dropdown-item" aria-current="page" to="sales-records/">All Sales Records</NavLink></li>
              <li><NavLink className="dropdown-item" aria-current="page" to="sales-records/filter/">Sales By Employee</NavLink></li>
            </div>
          </div>
          <h5>||</h5>
          <div className="dropdown">
            <button className="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
              Inventory
            </button>
            <div className="dropdown-menu dropdown-menu-dark" aria-labelledby="dropdownMenuButton2">
              <li><NavLink className="dropdown-item" aria-current="page" to="manufacturers/new/">New Manufacturer</NavLink></li>
              <li><NavLink className="dropdown-item" aria-current="page" to="manufacturers/">All Manufacturers</NavLink></li>
              <li><NavLink className="dropdown-item" aria-current="page" to="models/new/">New Vehicle Model</NavLink></li>
              <li><NavLink className="dropdown-item" aria-current="page" to="models/">All Models</NavLink></li>
              <li><NavLink className="dropdown-item" aria-current="page" to="automobiles/new/">New Automobile</NavLink></li>
              <li><NavLink className="dropdown-item" aria-current="page" to="automobiles/">All Automobiles</NavLink></li>
            </div>
          </div>
          <h5>||</h5>
          <div className="dropdown">
            <button className="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
              Service
            </button>
            <div className="dropdown-menu dropdown-menu-dark open" aria-labelledby="dropdownMenuButton2">
              <li><NavLink className="dropdown-item" aria-current="page" to="technicians/new/">Create a Technician</NavLink></li>
              <li><NavLink className="dropdown-item" aria-current="page" to="technicians/">All Technician</NavLink></li>
              <li><NavLink className="dropdown-item" aria-current="page" to="services/new">Create Service</NavLink></li>
              <li><NavLink className="dropdown-item" aria-current="page" to="/services/history/">Service History</NavLink></li>
              <li><NavLink className="dropdown-item" aria-current="page" to="services/">Service Appointments</NavLink></li>
            </div>
          </div>
          <h5>||</h5>
        </div>
      </nav>
    </>
  )
}

export default Nav;


