import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Component } from "react";
import SalesRecordForm from "./SalesRecordForm";
import SalesRepForm from "./SalesRepForm";
import SalesRecordList from "./SalesRecordList";
import SalesRecordFiltered from "./SalesRecordFiltered";
import CustomerForm from "./CustomerForm";
import ModelsList from "./ModelsList";
import ModelsForm from "./ModelsForm";
import MainPage from "./MainPage";
import Nav from "./Nav";
import ManufacturersList from "./ManufacturersList";
import ManufacturerForm from "./ManufacturerForm";
import VehicleInventory from "./VehicleInventory";
import ServiceAppointmentList from "./ServiceAppointmentList";
import ServiceAppointmentForm from "./ServiceAppointmentForm";
import ServiceTechnicianForm from "./ServiceTechnicianForm";
import ServiceTechList from "./ServiceTechList";
import VehicleForm from "./VehicleForm";
import ServiceHistory from "./ServiceHistory";

import "./index.css";
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  async componentDidMount() {
    Promise.all([
      fetch("http://localhost:8090/api/sales-records/"),
      fetch("http://localhost:8080/api/technicians/"),
      fetch("http://localhost:8100/api/manufacturers/"),
      fetch("http://localhost:8100/api/models/"),
      fetch("http://localhost:8080/api/appointments/"),
      fetch("http://localhost:8100/api/automobiles/"),
    ])
      .then(
        ([sales_records, manufacturers, models, automobiles, technicians, appointments]) => {
        return Promise.all([
          sales_records.json(),
          manufacturers.json(),
          models.json(),
          automobiles.json(),
          technicians.json(),
          appointments.json(),
        ])
      })
      .then(
        ([
          sales_records,
          manufacturers,
          models,
          automobiles,
          technicians,
          appointments,
        ]) => {
          this.setState(sales_records);
          this.setState(manufacturers);
          this.setState(models);
          this.setState(automobiles);
          this.setState(technicians);
          this.setState(appointments);
        }
      );
  }

  render() {
    return (
      <BrowserRouter>
        <Nav />
        <div className="container">
          <Routes>
            <Route path="/" element={<MainPage />} />
            <Route path="sales-persons/">
              <Route path="new/" element={<SalesRepForm />} />
            </Route>
            <Route path="customers/">
              <Route path="new/" element={<CustomerForm />} />
            </Route>
            <Route path="sales-records/">
              <Route
                path=""
                element={
                  <SalesRecordList salesRecords={this.state.sales_records} />
                }
              />
              <Route path="new/" element={<SalesRecordForm />} />
              <Route path="filter/" element={<SalesRecordFiltered salesRecords={this.state.sales_records} /> } />
            </Route>
            <Route path="models/">
              <Route
                path=""
                element={<ModelsList vehicleList={this.state.models} />}
              />
              <Route path="new/" element={<ModelsForm />} />
            </Route>
            <Route path="manufacturers/">
              <Route
                path=""
                element={
                  <ManufacturersList manuList={this.state.manufacturers} />
                }
              />
              <Route path="new/" element={<ManufacturerForm />} />
            </Route>
            <Route path="automobiles/">
              <Route
                path=""
                element={<VehicleInventory InventoryList={this.state.autos} />}
              />
              <Route path="new/" element={<VehicleForm />} />
            </Route>
            <Route path="/services/">
              <Route
                path=""
                element={
                  <ServiceAppointmentList
                    AppointmentList={this.state.appointments}
                  />
                }
              />
              <Route
                path="history/"
                element={
                  <ServiceHistory AppointmentList={this.state.appointments} />
                }
              />
              <Route path="new/" element={<ServiceAppointmentForm />} />
            </Route>
            <Route path="technicians">
              <Route
                path=""
                element={
                  <ServiceTechList TechnicianList={this.state.technicians} />
                }
              />
              <Route path="new/" element={<ServiceTechnicianForm />} />
            </Route>
          </Routes>
        </div>
      </BrowserRouter>
    );
  }
}
