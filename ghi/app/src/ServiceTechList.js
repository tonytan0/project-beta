import { NavLink } from 'react-router-dom'

function ServiceTechList({ TechnicianList }) {
  return (
    <>
      <h1>
        Technician List
      </h1>
      <table className="table bdr table-hover table-info table-striped">
        <thead>
          <tr>
            <th>Tech Name</th>
            <th>Employee Number</th>
          </tr>
        </thead>
        <tbody>
          {TechnicianList && TechnicianList.map(tech => {
            return (
              <tr key={tech.id}>
                <td>{tech.name}</td>
                <td>{tech.employee_number}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <button type="button" className=" btn btn-outline-dark btn-sm float-end">
        <NavLink className="fs-6" aria-current="page" to="/technicians/new">Add a Technician</NavLink>
      </button>
    </>
  );
}

export default ServiceTechList