# CarCar
```
Team:

* Person 1 - Adrian Dorado - Sales
* Person 2 - Tony Tan - Service
```
## Design
```
CarCar is an app for dealerships to manage their customers, Sales Records, employees, and service.

To get started on the sales side of the app, you will need to create a manufacturer, a vehicle model, and then an automobile using those two previous 
fields + color and year.
Customers will need to be created, along with a sales person to create a Sales Record (the Sales Record also requires an automobile to be created).
Sales Records can be viewed as a full list or filtered by sales person via Case-Sensitive text searches.

To get started with the service side of the app, you will need to create a techinican and then assign that technician to a service in Create Service. A list of
technicians can be viewed on the All Technicians page.
The service history can be viewed on the Service History page and the active appointments can be viewed, cancelled, or finished in Service Appointments
----------------------------------------------------
The design of the app, aesthetically, uses bootstrap for all of the CSS with one custom class to round the corners of the tables. The Nav bar has been switched to drop-down menus for each of the specific parts of the app for easier use.
We agreed on terms that would be verbose as possible without making them too bloated: Customer, AutomobileVO, Appointment, Technician, SalesPerson, and SalesRecord. We tried to make the React side of the app as light as possible in regards to the code (Dry code is best, right?) by using promises instead of a ton of componentDidMount functions. 
```
## Service microservice
```
Explain your models and integration with the inventory
microservice, here.

The Service microservice consists of three models:
----------------------------------------------------
    1. Appointment:
        This model handles the vin, customer's name the service reason along with the technican who is working on the appointment.

    2. Technician
        This model handles the employee's name and number.

    3. AutomobileVO
        This Value Object represents the automobile's vin. 

The Service microservice has an appointment form to create a service. It also has a form to create a new technician with just their name and employee number. 
With the appointment form, you can go into a list where you can cancel the service appointment to get them off the list and "finish" the service appointment.



## Sales microservice

```
Explain your models and integration with the inventory
microservice, here.

The Sales microservice consists of four models:
----------------------------------------------------
1. AutomobileVO: This model handles the integration with the inventory microservice
    by creating a value object with the unique property of "vin". This allows me 
    to pull the individual values of "automobile" from the inventory microservice,
    making it possible to make a foreign key relationship between the SalesRecord and Automobile, from inventory_rest. We accomplish this by making a function
    that will poll the inventory microservice for information on automobiles, using
    the vin number specifically. 

2. Customer: The customer model allows us to create a customer using three
    properties: name, address, and phone number. This will also relate to
    SalesRecord

3. SalesPerson: The SalesPerson model allows us to create a sales person with three
    properties: name, employee_number, and sales_made. This relates to
    SalesRecord

4. SalesRecord: The SalesRecord model allows us to create a sales record using the
    three other models as ForeignKey relationships, with the additional property of
    sales_price
```
