from email import contentmanager
from pyexpat import model
from urllib import response
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import pkg_resources

from common.json import ModelEncoder, DateEncoder
import json

from service_rest.models import Appointment, Technician

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        'id',
        'name',
        'employee_number',
    ]


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "customer_name",
        "date",
        "time",
        "reason",
        "vip",
        'finished',
    ]

    def get_extra_data(self, o):
        return { "technician": o.technician.name, "vin": o.vin}


# Create your views here.


# list of techs
#  will need encoder for techs
@require_http_methods(["GET", "POST"])
def list_techicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()

        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
            # safe = False,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)

            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )

        except:
            response = JsonResponse(
                {"technician": "Try to make a technician again, Buddy!"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE", "GET", "PUT"])
def detail_technician(request, pk):
    if request.method == "DELETE":
        technician = Technician.objects.get(id=pk)
        technician.delete()
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )
    elif request.method == "GET":
        technician = Technician.objects.get(id=pk)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        Technician.objects.filter(id=pk).update(**content)
        technician = Technician.objects.get(id=pk)
        return (JsonResponse(technician, encoder=TechnicianEncoder, safe=False),)


@require_http_methods(["GET", "POST"])
def list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.filter(finished=False)

        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentDetailEncoder,
            safe=False,
        )
    else: #POST
        content = json.loads(request.body)
        content = {
            **content,
            "technician": Technician.objects.get(pk=content["technician"])
        }
        appointments = Appointment.objects.create(**content)

        return JsonResponse(
            appointments,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def detail_appointment(request, pk):
    if request.method == "DELETE":
        # appointment = Appointment.objects.get(id=pk)
        # appointment.delete()
        # return JsonResponse(
        #     appointment,
        #     encoder=AppointmentDetailEncoder,
        #     safe=False,
        # )
        count, _ = Appointment.objects.filter(id=pk).delete()

        return JsonResponse({"deleted": count > 0})
    elif request.method == "GET":
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )
    else:  # PUT
        content = json.loads(request.body)
        Appointment.objects.filter(id=pk).update(**content)
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(appointment, encoder=AppointmentDetailEncoder, safe=False)
